$(function() {
  var windowResize = function() {
    if($(window).width() >= 1024) {
      largeActive = true;
      $('.slider__interchange.show-for-large-up > .cycle-slideshow').cycle('resume');
      $('.slider__interchange.hide-for-large-up > .cycle-slideshow').cycle('pause');
    } else if($(window).width() < 1024) {
      largeActive = false;
      $('.slider__interchange.show-for-large-up > .cycle-slideshow').cycle('pause');
      $('.slider__interchange.hide-for-large-up > .cycle-slideshow').cycle('resume');
    }
  }

  $(window).on('resize', windowResize);
  windowResize();
});

