$(function() {
  $(document).foundation();

  $('.navigation__primary').headroom({
    tolerance: {
      up: 20,
      down: 5
    },
        classes : {
        // when element is initialised
        initial : "headroom",
        // when scrolling up
        pinned : "headroom--pinned",
        // when scrolling down
        unpinned : "headroom--unpinned",
        // when above offset
        top : "headroom--top",
        // when below offset
        notTop : "headroom--not-top"
    },
    offset : 105
  });

  new WOW().init();
});

$('[data-reveal]').on('open.fndtn.reveal', function () {
  $('body').addClass('modal-open');
});

$('[data-reveal]').on('close.fndtn.reveal', function () {
  $('body').removeClass('modal-open');
});

$(document).on('oanimationend animationend webkitAnimationEnd', '.animated', function() {
  $(this).removeClass('animated');
});

$(document).on('click', '.step-back', function(e) {
  e.preventDefault();
  window.history.go(-1);
});
