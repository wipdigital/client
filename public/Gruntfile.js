module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      index: {
        src: ['tmp/index.js'],
        dest: 'build/index.min.js'
      },
      async: {
        src: ['tmp/async.js'],
        dest: 'build/async.min.js'
      },
    },
    concat: {
      options: {
        seperator: ';'
      },
      index: {
        src: [
          //Core dependencies
          'bower_components/modernizr/modernizr.js',
          'bower_components/jquery/dist/jquery.min.js',
          'bower_components/jquery.easing/js/jquery.easing.min.js',
          'bower_components/foundation/js/foundation/foundation.js',
          'bower_components/foundation/js/foundation/foundation.reveal.js',
          //'bower_components/foundation/js/foundation/foundation.abide.js',

          //Optional dependencies
          'bower_components/jquery-cycle2/build/jquery.cycle2.min.js',
          'bower_components/wow/dist/wow.js',
          'bower_components/magnify/dist/js/jquery.magnify.js',

          //Blog dependencies
          'bower_components/isotope/dist/isotope.pkgd.js',
          
          //Navigation components
          'bower_components/classie/classie.js',
          'bower_components/headroom.js/dist/headroom.js',
          'bower_components/headroom.js/dist/jQuery.headroom.js',
          'js/components/navigation/primary.js',

          //Forms components
          'bower_components/Snap.svg/dist/snap.svg.js',
          'js/components/forms/standard.js',

          //Sliders components
          'js/components/sliders/standard.js',
          //'js/components/sliders/interchange.js',
          
          //Main JS file
          'js/index.js'
        ],
        dest: 'tmp/index.js'
      },
      async: {
        src: ['bower_components/fastclick/lib/fastclick.js', 'js/async.js'],
        dest: 'tmp/async.js'
      },
    },
    sass: {
      index: {
        options: {
          sourceMap: true,
          includePaths: ['bower_components/foundation/scss']
        },
        files: {
          'tmp/index.css': 'scss/index.scss'
        }
      }
    },
    compass: {
      build: {
        options: {
          sassDir: 'scss',
          cssDir: 'tmp',
          specify: 'scss/index.scss'
        }
      }
    },
    cssmin: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        files: {
          'build/index.min.css': ['tmp/index.css']
        }
      }
    },
    watch: {
      options: {
        livereload: true
      },
      scss: {
        files: ['scss/*.scss', 'scss/**/*.scss'],
        tasks: ['build:css']
      },
      js: {
        files: ['js/*.js', 'js/**/*.js'],
        tasks: ['build:js']
      }
    }
  });

  // Load the plugins that provide the task(s).
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sass');

  // Default task(s).
  grunt.registerTask('build:js', ['concat', 'uglify']);
  grunt.registerTask('build:css', ['sass', 'cssmin']);
  grunt.registerTask('default', ['build:js','build:css']);
};
