<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title>404 - Page not found!</title>

    <!--
     __    __  ___  __           _____    __     ___  __    ___  ___   __    __  __  __ 
    / / /\ \ \/___\/__\  /\ /\   \_   \/\ \ \   / _ \/__\  /___\/ _ \ /__\  /__\/ _\/ _\
    \ \/  \/ //  // \// / //_/    / /\/  \/ /  / /_)/ \// //  // /_\// \// /_\  \ \ \ \ 
     \  /\  / \_// _  \/ __ \  /\/ /_/ /\  /  / ___/ _  \/ \_// /_\\/ _  \//__  _\ \_\ \
      \/  \/\___/\/ \_/\/  \/  \____/\_\ \/   \/   \/ \_/\___/\____/\/ \_/\__/  \__/\__/
     -->

  </head>
  <body>
    <h1>404 - Page not found!</h1>
  </body>
</html>
