<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>@if($page->permalink == 'home'){{ $page->seo_title ?: DB::table('settings')->where('key', '=', 'site_name')->pluck('value') }}@else{{ $title }} - {{ DB::table('settings')->where('key', '=', 'site_name')->pluck('value') }}@endif</title>
    <meta name="description" content="{{ DB::table('settings')->where('key', '=', 'site_description')->pluck('value') }}">

    <meta property="og:image" content="{{ Config::get('app.url') }}/images/og.png">
    <meta property="og:description" content="{{ DB::table('settings')->where('key', '=', 'site_description')->pluck('value') }}">

    <!--
     __    __  ___  __           _____    __     ___  __    ___  ___   __    __  __  __ 
    / / /\ \ \/___\/__\  /\ /\   \_   \/\ \ \   / _ \/__\  /___\/ _ \ /__\  /__\/ _\/ _\
    \ \/  \/ //  // \// / //_/    / /\/  \/ /  / /_)/ \// //  // /_\// \// /_\  \ \ \ \ 
     \  /\  / \_// _  \/ __ \  /\/ /_/ /\  /  / ___/ _  \/ \_// /_\\/ _  \//__  _\ \_\ \
      \/  \/\___/\/ \_/\/  \/  \____/\_\ \/   \/   \/ \_/\___/\____/\/ \_/\__/  \__/\__/

      Website design and development by Work In Progress (http://www.workinprogress.com.au)
     -->

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    @yield('inline_meta')

    @if(App::environment('local'))
    <link rel="stylesheet" href="/tmp/index.css">  
    @else
    <link rel="stylesheet" href="/build/index.min.css">
    @endif

    @yield('inline_css')
  </head>
  <body @if(isset($page)) class="{{ str_singular($page->template->src) }}" @endif >
    @yield('layout')

    <script src="/build/index.min.js"></script>
    <script src="/build/async.min.js" async></script>
    <script src="//use.typekit.net/zgv4mea.js"></script>
    <script>
      try{Typekit.load();}catch(e){}
      $(function() {
        document.body.className = document.body.className + " body-active";
      });
    </script>
    <script>$(function() { document.body.className = document.body.className + " body-active"; });</script>
    @yield('inline_js')
    @if(App::environment('local'))
    <script src="//localhost:35729/livereload.js"></script>
    @endif
  </body>
</html>
